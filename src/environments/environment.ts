// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDHRLdB4IrKIDyYJifPmzjNYOwsa_J6gZc",
    authDomain: "parcial01-270ad.firebaseapp.com",
    databaseURL: "https://parcial01-270ad.firebaseio.com",
    projectId: "parcial01-270ad",
    storageBucket: "parcial01-270ad.appspot.com",
    messagingSenderId: "323168543034",
    appId: "1:323168543034:web:5afecb1c9c9bcdb0013003",
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
