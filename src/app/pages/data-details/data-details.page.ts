import { Component, OnInit } from '@angular/core';
import { Data } from '../../models/data';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class DataDetailsPage implements OnInit {
  
  data: Data[];
  constructor(private route: ActivatedRoute, private nav: NavController,
    private dataService: DataService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.dataService.getAllData().subscribe(res => {
      console.log('Data', res);
      this.data = res;
    });
  }

}
